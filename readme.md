Sub-event
=======================

Parent process call a child process, the child process complete sending an event (not type, default scope).
The parent process get the child node completed twice and trigger twice the following task.

![parent](src/main/resources/com/myspace/sub_event/sub-event.parent-svg.svg)

![child](src/main/resources/com/myspace/sub_event/sub-event.child-svg.svg)